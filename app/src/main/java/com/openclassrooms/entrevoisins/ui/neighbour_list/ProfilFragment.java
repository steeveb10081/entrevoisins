package com.openclassrooms.entrevoisins.ui.neighbour_list;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;

import com.openclassrooms.entrevoisins.R;
import com.openclassrooms.entrevoisins.model.Neighbour;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfilFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfilFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM = "paramNeighbour";

    // TODO: Rename and change types of parameters
    private Neighbour mParamNeighbour;
    TextView tex_profil1;
    TextView tex_profil2;
    TextView tex_location;
    TextView tex_phone;
    TextView tex_web;
    TextView libelle_apropos;
    ImageView img_porfil;
    ImageView back_image;

    public ProfilFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param item Parameter 1.
     * @return A new instance of fragment ProfilFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfilFragment newInstance(Neighbour item) {
        ProfilFragment fragment = new ProfilFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, item);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParamNeighbour = (Neighbour) getArguments().getSerializable(ARG_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profil, container, false);

    }


    @Override
    public void onViewCreated(View view,  Bundle savedInstanceState) {
        tex_profil1= (TextView) getView().findViewById(R.id.nom_profil);
        tex_profil2= (TextView) getView().findViewById(R.id.nom_profil2);
        tex_location= (TextView) getView().findViewById(R.id.id_location);
        tex_phone= (TextView) getView().findViewById(R.id.id_phone);
        tex_web= (TextView) getView().findViewById(R.id.id_web);
        libelle_apropos= (TextView) getView().findViewById(R.id.id_libelle_apropos);
        img_porfil=getView().findViewById(R.id.img_porfil);

        back_image = getView().findViewById(R.id.id_back_image);

      Glide.with(getContext())
              .load(mParamNeighbour.getAvatarUrl())
              .into(img_porfil);

        tex_profil1.setText(mParamNeighbour.getName());
        tex_profil2.setText(mParamNeighbour.getName());
        tex_location.setText(mParamNeighbour.getAddress());
        tex_phone.setText(mParamNeighbour.getPhoneNumber());

        tex_web.setText("www.facebook.fr/"+mParamNeighbour.getName());
        libelle_apropos.setText(mParamNeighbour.getAboutMe());

        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

    }



}