package com.openclassrooms.entrevoisins.ui.neighbour_list;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.openclassrooms.entrevoisins.R;
import com.openclassrooms.entrevoisins.model.Neighbour;

public class ProfilActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        if(savedInstanceState == null) {
            Neighbour item= (Neighbour) getIntent().getSerializableExtra("MyNeighbour");
            getSupportFragmentManager().beginTransaction().replace(R.id.myFrame,  ProfilFragment.newInstance(item)).commit();
        }
    }
}